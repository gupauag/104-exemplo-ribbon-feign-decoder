package br.com.mastertech.carro.carro.service;

import br.com.mastertech.carro.carro.exceptions.CarroNotFoundException;
import br.com.mastertech.carro.carro.model.Carro;
import br.com.mastertech.carro.carro.repository.CarroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CarroService {

    @Autowired
    private CarroRepository carroRepository;

    public Carro create(Carro carro) {
        return carroRepository.save(carro);
    }

    public Carro getByPlaca(String placa) {
        Optional<Carro> byId = carroRepository.findById(placa);

        if(!byId.isPresent()) {
            throw new CarroNotFoundException();
        }

        return byId.get();
    }


}
